var express=require('express');
var router=express.Router();
var user=require('../models/user'); //引入数据集/表  里面有字段 也有方法封装
router.get('/', function(req,res,next){

res.send('兄弟 新年快乐');

});

router.post('/register', function(req,res,next){
   if(!req.body.username){

    res.json({status:1,message:"用户名为空"})
   }
   if(!req.body.password){

    res.json({status:1,message:"密码为空"})
   }
   if(!req.body.userMail){

    res.json({status:1,message:"邮箱为空"})
   }
   if(!req.body.userPhone){

    res.json({status:1,message:"手机为空"})   //先进行验证
   }
   //如果都没有空的 就进行传参回调 执行方法
   user.findByUsername(req.body.username,function(err,userSave){
    if(userSave.length!=0){
      res.json({status:1,message:"用户名已经注册"})
    }else{
    var registerUser=new user({
      username:req.body.username,
      password:req.body.password,
      userMail:req.body.userMail,
      userphone:req.body.userPhone,
      userAdmin:0,
      userPower:0,
      userStop:0,
    })
    //把接受的对象传进去。
     registerUser.save(function(){
         res.json({status:0, message:"注册成功"})

     })

    }
})

});

module.exports=router;